# Image of Grid Map to 2D Array Repository README #

This README document covers every step which is necessary to get your application up and running.

### What is this repository for? ###

This project is used for creating simple Bomberman levels from image or video.
The captured frame is analyzed using OpenCV library.
Maps should be drawn on white sheet of paper using black marker. Map is a grid like structure filled with blank cells, triangles and rectangles.

Detected grid is translated to a String, for example:

"2 3 012012"

The first digit is grid width, the second grid height, last digits stores the information of grid cells.

Cell information:

0 - blank cell

1 - triangle

2 - rectangle

![Project in action](https://bytebucket.org/trodekas/image-of-grid-map-to-2d-array/raw/56cc568490eb0d303e85094860137bac0e91aab2/images/readme/ezgif-1920128833.gif?token=20f9409b5953d72fcfa26c25d54388be92db34e5)

### How to set up? ###

* Clone repository: git clone https://bitbucket.org/trodekas/image-of-grid-map-to-2d-array
* Once the project was cloned, start IntelliJ IDEA:
* On welcome screen of IntelliJ IDEA, press "Import Project".
* Select "Create project from existing sources"
* Press next next next...
* Wait for the project to build...
* When the project has loaded, go to *Run -> Edit Configurations...*
* Create new *Application* type configuration.
* Set the following properties:
* * *Main class:* testDummy.TestDummy
* Then add required libraries for the project:
* * Add OpenCV libraries: File -> Project Structure -> Project Settings -> Libraries -> New Project Library (+), select ./lib/opencv-310.jar file.
* * Add OpenCV native library location: File -> Project Structure -> Project Settings -> Libraries -> New Project Library (+), select ./lib/x64/opencv_java310.dll. IntelliJ automatically recognizes this as a native library (no need to set VM argument).
* * Press *OK*.
* Run the project.

### Project structure ###

All production java code files are placed in *src/* directory.
All images used are stored in *images/*.

Saulius Stankevičius