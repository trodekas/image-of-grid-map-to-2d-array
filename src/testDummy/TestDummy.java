package testDummy;

import opencvModule.Core;
import testDummy.components.*;

import testDummy.components.Frame;

import java.util.ArrayList;

/**
 * Created by Saulius Stankevicius
 *
 */
public class TestDummy
{
    private static Core core;

    public TestDummy()
    {
        ImagePanel imagePanel = new ImagePanel();

        TestDummy.core = new Core(imagePanel, this.loadImagePathBuffer());
        new Frame(imagePanel, core);

    }

    private ArrayList<String> loadImagePathBuffer()
    {
        ArrayList<String> imageBuffer = new ArrayList<>();
        imageBuffer.add("images\\testDummy\\blackCell.png");
        imageBuffer.add("images\\testDummy\\brickWall.png");
        imageBuffer.add("images\\testDummy\\steelWall.png");

        return imageBuffer;
    }

    public static void main(String[] args)
    {
        new TestDummy();
    }
}
