package testDummy.components;

import opencvModule.imageOut.ImagePanelInterface;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Saulius Stankevicius
 * Class for displaying the image
 */
public class ImagePanel extends JPanel implements ImagePanelInterface
{
    private Image imageToDraw = null;

    private final Color frameColor = Color.red;

    private final int preferredWidth = 620;
    private final int preferredHeight = 460;

    @Override
    protected void paintComponent(Graphics g)
    {
        g.setColor(this.frameColor);
        g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);

        if(this.imageToDraw != null)
        {
            g.drawImage(this.imageToDraw, 0, 0, this);
        }
    }

    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(this.preferredWidth, this.preferredHeight);
    }

    @Override
    public void refreshImage(Image image)
    {
        this.imageToDraw = image;
        this.repaint();
    }
}
