package testDummy.components;

import opencvModule.Core;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;

import java.util.Locale;

/**
 * Created by Saulius Stankevicius.
 * Class for displaying a frame of GUI
 */
public class Frame extends JFrame
{
    private ImagePanel imagePanel = null;
    private static Core core;

    private JButton captureButton = new JButton("Start Capture");
    private JButton loadImageButton = new JButton("Load Image");

    private boolean run = false;

    public Frame(ImagePanel imagePanel, Core core)
    {
        this.imagePanel = imagePanel;
        Frame.core = core;

        this.initialization();
    }

    /**
     * Sets up the frame
     */
    private void initialization()
    {
        this.setLocale(Locale.US);
        this.setTitle("Test Dummy");

        this.constructFrame();
        this.addActionListeners();

        setVisible(true);
        setResizable(false);
        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Adds components to the frame
     */
    private void constructFrame()
    {
        Container container = this.getContentPane();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        container.add(this.imagePanel);
        container.add(this.captureButton);
        container.add(this.loadImageButton);
    }

    /**
     * Adds action listeners to the components
     */
    private void addActionListeners()
    {
        this.captureButton.addActionListener(this::captureButtonAction);
        this.loadImageButton.addActionListener(this::loadImageButtonAction);
    }

    /**
     * Capture button action, if opencvModule.Core is not running opens the camera and start to analyze video feed
     * else stops the opencvModule.Core
     * @param e action event
     */
    private void captureButtonAction(ActionEvent e)
    {
        if(!run)
        {
            captureButton.setText("Stop Capture");
            this.run = true;
            core.analyze();
        }
        else
        {
            captureButton.setText("Start Capture");
            this.run = false;
            System.out.println(core.getMapData());
            core.stop();
        }
    }

    /**
     * Opens the file chooser, when the valid file is chosen analyzes the image
     * @param e action event
     */
    private void loadImageButtonAction(ActionEvent e)
    {
        if(!run)
        {
            String imageToAnalyze = this.chooseImage();
            if(imageToAnalyze != null)
            {
                this.run = true;
                core.analyze(imageToAnalyze);

                System.out.println(core.getMapData());
                core.stop();
                this.run = false;
            }
            else
            {
                JOptionPane.showMessageDialog(Frame.this, "Invalid file", "Warning", JOptionPane.WARNING_MESSAGE);
            }
        }
        else
        {
            JOptionPane.showMessageDialog(Frame.this, "Another instance of this process is running", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Opens the file chooser for selecting an image, returns selected image path
     * @return selected image path
     */
    private String chooseImage()
    {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choose map image to scan");
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir") + File.separator));
        FileNameExtensionFilter imageFilter = new FileNameExtensionFilter(
                "Image files", ImageIO.getReaderFileSuffixes());
        fileChooser.addChoosableFileFilter(imageFilter);
        fileChooser.setAcceptAllFileFilterUsed(false);

        int returnValue = fileChooser.showOpenDialog(Frame.this);
        if(returnValue == fileChooser.APPROVE_OPTION)
        {
            File file = fileChooser.getSelectedFile();

            return file.getPath();
        }

        return null;
    }
}
