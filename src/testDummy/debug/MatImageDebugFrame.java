package testDummy.debug;

import opencvModule.core.ContourUtilities;
import opencvModule.dataStructure.MatImage;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import testDummy.components.ImagePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by Saulius Stankevičius.
 * Class for easy display of MatImage
 */
public class MatImageDebugFrame extends JFrame
{
    private ImagePanel imagePanel;
    private Container container = new Container();

    public MatImageDebugFrame(String matName)
    {
        setTitle(matName);
        this.constructFrame();

        setVisible(true);
        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Constructs a frame
     */
    private void constructFrame()
    {
        this.container = this.getContentPane();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        this.imagePanel = new ImagePanel();
        container.add(imagePanel);
    }

    /**
     * Displays MatImage in the frame
     * @param matImage mat to display
     */
    public void drawMat(MatImage matImage)
    {
        this.imagePanel.refreshImage(matImage.getImage());
    }

    public void drawImage(BufferedImage bufferedImage)
    {
        this.imagePanel.refreshImage(bufferedImage);
    }

    public void drawMat(MatImage matImage, MatOfPoint contour, Scalar color)
    {
        ArrayList<MatOfPoint> contourArray = new ArrayList<>();
        contourArray.add(contour);

        this.drawMat(matImage, contourArray, color);
    }
    /**
     * Draws contours on a MatImage and displays it in the frame
     * @param matImage mat to display
     * @param contours contours to draw
     * @param color color in which to draw contours
     */
    public void drawMat(MatImage matImage, ArrayList<MatOfPoint> contours, Scalar color)
    {
        MatImage matToDraw = new MatImage();
        matImage.copyTo(matToDraw);
        if(matToDraw.channels() < 3)
        {
            Imgproc.cvtColor(matToDraw, matToDraw, Imgproc.COLOR_GRAY2RGB);
        }

        for(int i = 0; i < contours.size(); i++)
        {
            Imgproc.drawContours(matToDraw, contours, i, color, 2);
            org.opencv.core.Point point = new org.opencv.core.Point(ContourUtilities.getXCoordinate(contours.get(i)),
                    ContourUtilities.getYCoordinate(contours.get(i)));
            Imgproc.putText(matToDraw, Integer.toString(i), point, org.opencv.core.Core.FONT_HERSHEY_PLAIN, 1, new Scalar(250, 0, 250), 2);
        }

        this.imagePanel.refreshImage(matToDraw.getImage());
    }
}
