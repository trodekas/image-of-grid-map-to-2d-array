package opencvModule.imageOut;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Saulius Stankevicius
 * Class for image handling
 */
public class ImageUtilities
{
    private ImageUtilities()
    {

    }

    /**
     * Loads an image
     * @param imagePath path to image
     * @return loaded image
     */
    public static BufferedImage loadImage(String imagePath)
    {
        try
        {
            BufferedImage tmp = ImageIO.read(new File(imagePath));
            return resize(tmp, 640, 480);
        }
        catch(IOException ex)
        {
            System.out.println("Unable to load image to analyze");
        }

        return null;
    }

    /**
     * Loads images
     * @param imagesPathForOverlay paths to the images
     * @return loaded images
     */
    public static ArrayList<BufferedImage> loadImages(ArrayList<String> imagesPathForOverlay)
    {
        ArrayList<BufferedImage> images = new ArrayList<>();
        images.clear();
        for(String path : imagesPathForOverlay)
        {
            try
            {
                images.add(ImageIO.read(new File(path)));
            }
            catch(IOException ex)
            {
                System.out.print("Error reading image");
            }
        }

        return images;
    }

    /**
     * Resizes BufferedImage into desired size
     * @param img image to resize
     * @param newWidth desired width
     * @param newHeight desired height
     * @return resized BufferedImage
     */
    public static BufferedImage resize(BufferedImage img, int newWidth, int newHeight)
    {
        Image tmp = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
        BufferedImage resizedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_3BYTE_BGR);

        Graphics2D g2d = resizedImage.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return resizedImage;
    }
}
