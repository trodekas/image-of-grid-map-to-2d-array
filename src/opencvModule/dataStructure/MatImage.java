package opencvModule.dataStructure;

import org.opencv.core.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;

/**
 * Created by Saulius Stankevicius
 * Class for Mat object with extended functionality
 */
public class MatImage extends Mat
{
    public MatImage()
    {
        super();
    }

    public MatImage(Mat mat)
    {
        super();
        mat.copyTo(this);
    }

    public MatImage(long addr)
    {
        super(addr);
    }

    public MatImage(int rows, int cols, int type)
    {
        super(rows, cols, type);
    }

    public MatImage(Size size, int type)
    {
        super(size, type);
    }

    public MatImage(int rows, int cols, int type, Scalar s)
    {
        super(rows, cols, type, s);
    }

    public MatImage(Size size, int type, Scalar s)
    {
        super(size, type, s);
    }

    public MatImage(Mat m, Range rowRange, Range colRange)
    {
        super(m, rowRange, colRange);
    }

    public MatImage(Mat m, Range rowRange)
    {
        super(m, rowRange);
    }

    public MatImage(Mat m, Rect roi)
    {
        super(m, roi);
    }

    /**
     * Constructs MatImage from BufferedImage
     * @param bufferedImage image to construct from
     */
    public MatImage(BufferedImage bufferedImage)
    {
        super(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8UC3);
        byte[] data = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
        this.put(0, 0, data);
    }

    /**
     * Construct MatImage from BufferedImage array
     * @param bufferedImages array of BufferedImages to construct from
     * @param cols in how many collumns to organize BufferedImages
     * @param rows in how many rows to organize BufferedImages
     */
    public MatImage(ArrayList<BufferedImage> bufferedImages, int cols, int rows)
    {
        super(bufferedImages.get(0).getHeight() * rows, bufferedImages.get(0).getWidth() * cols, CvType.CV_8UC3);

        BufferedImage stitchedImage = this.getStitchedImages(bufferedImages, cols, rows);
        if(stitchedImage != null)
        {
            byte[] data = ((DataBufferByte) stitchedImage.getRaster().getDataBuffer()).getData();
            this.put(0, 0, data);
        }
    }

    /**
     * Stitches BufferedImages into one big BufferedImage
     * @param bufferedImages array to stitch
     * @param cols in how many collumns to organize BufferedImages
     * @param rows in how many rows to organize BufferedImages
     * @return stitched BufferedImage
     */
    private BufferedImage getStitchedImages(ArrayList<BufferedImage> bufferedImages, int cols, int rows)
    {
        if(bufferedImages.size() == rows * cols && bufferedImages.size() > 1)
        {
            int type = bufferedImages.get(0).getType();
            int imageBlockWidth = bufferedImages.get(0).getWidth();
            int imageBlockHeight = bufferedImages.get(0).getHeight();

            BufferedImage stitchedImage = new BufferedImage(imageBlockWidth * cols, imageBlockHeight * rows, type);

            int i = 0;
            for (int y = 0; y < rows; y++) {
                for (int x = 0; x < cols; x++) {
                    stitchedImage.createGraphics().drawImage(bufferedImages.get(i), imageBlockWidth * x, imageBlockHeight * y, null);
                    i++;
                }
            }

            return stitchedImage;
        }
        else
        {
            return null;
        }
    }

    /**
     * Converts Mat to Image
     * @return converted image
     */
    private Image convertMatToImage()
    {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if(this.channels() > 1)
        {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = this.channels() * this.cols() * this.rows();
        byte[] b = new byte[bufferSize];
        this.get(0, 0, b);
        BufferedImage image = new BufferedImage(this.cols(), this.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);

        return image;
    }

    /**
     * Converts and returns Image
     * @return image
     */
    public Image getImage()
    {
        return this.convertMatToImage();
    }
}
