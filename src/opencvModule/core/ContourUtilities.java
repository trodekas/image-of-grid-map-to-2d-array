package opencvModule.core;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import opencvModule.dataStructure.MatImage;

/**
 * Created by Saulius Stankevicius
 * Class for getting useful information from contours (MatOfPoint)
 */
public class ContourUtilities
{
    private ContourUtilities()
    {

    }

    /**
     * Finds four bounding points of the given contour
     * @param matOfPoint contour
     * @return four bounding points
     */
    public static MatOfPoint getBoundingPoints(MatOfPoint matOfPoint)
    {
        Point[] pointToMat = new Point[4];

        pointToMat[0] = new Point(Double.MAX_VALUE, Double.MAX_VALUE);
        pointToMat[1] = new Point(0, Double.MAX_VALUE);
        pointToMat[2] = new Point(0, 0);
        pointToMat[3] = new Point(Double.MAX_VALUE, 0);

        Point[] points = matOfPoint.toArray();
        for (Point point : points) {
            double total = point.x + point.y;
            double difference = point.x - point.y;

            if (total < pointToMat[0].x + pointToMat[0].y) pointToMat[0] = point;
            if (difference > pointToMat[1].x - pointToMat[1].y) pointToMat[1] = point;

            if (total > pointToMat[2].x + pointToMat[2].y) pointToMat[2] = point;
            if (difference < pointToMat[3].x - pointToMat[3].y) pointToMat[3] = point;
        }

        return new MatOfPoint(pointToMat);
    }

    /**
     * Finds out if the given contour has a larger area than a value of the passed argument
     * @param contour contour which area will be compared
     * @param area area to compare to
     * @return true if given contour area is larger
     */
    public static boolean checkIfContourAreaLargerThan(MatOfPoint contour, double area)
    {
        return contour != null && getContourArea(contour) > area;
    }

    /**
     * Finds contours which has a similar area to a passed argument
     * @param contours contours list
     * @param area area to compare
     * @param margin value by which contour area can be different
     * @return a list of contours which has a similar size
     */
    public static ArrayList<MatOfPoint> getContoursWithSimilarAreaTo(ArrayList<MatOfPoint> contours, double area, double margin)
    {
        ArrayList<MatOfPoint> similarAreaContours = new ArrayList<>();

        for(MatOfPoint contour : contours)
        {
            if(Math.abs(area - getContourArea(contour)) < area * margin)
            {
                similarAreaContours.add(contour);
            }
        }

        return similarAreaContours;
    }

    /**
     * Finds a median contour area
     * @param contours list of contours
     * @return median area value
     */
    public static Double getMedianContourArea(ArrayList<MatOfPoint> contours)
    {
        if(contours.size() < 1) { return 0.0; }

        sortContoursByArea(contours);
        int middle = contours.size() / 2;
        if(contours.size() % 2 == 0)
        {
            return (getContourArea(contours.get(middle - 1)) + getContourArea(contours.get(middle))) / 2;
        }
        else
        {
            return getContourArea(contours.get(middle));
        }
    }

    /**
     * Find an average contour area
     * @param contours list of contours
     * @return average area value
     */
    public static Double getAverageContourArea(ArrayList<MatOfPoint> contours)
    {
        if(contours.size() < 1) { return 0.0; }

        double totalArea = 0;
        for(MatOfPoint contour : contours)
        {
            totalArea += getContourArea(contour);
        }

        return  totalArea / (double)contours.size();
    }

    /**
     * Sorts the list of contours by their area
     * @param contours list of contours
     */
    public static void sortContoursByArea(ArrayList<MatOfPoint> contours)
    {
        Collections.sort(contours, new CompareByArea());
    }

    /**
     * Approximates given contours, removes redundant points from the contour
     * @param contours contours to approximate
     * @param approximationAmount by how much to approximate
     * @return approximated contour list
     */
    public static ArrayList<MatOfPoint> getApproximatedContours(ArrayList<MatOfPoint> contours, double approximationAmount)
    {
        ArrayList<MatOfPoint> approximatedContours = new ArrayList<>();
        for(MatOfPoint contour : contours)
        {
            approximatedContours.add(getApproximatedContour(contour, approximationAmount));
        }

        return approximatedContours;
    }

    /**
     * Approximates given contour, removes redundant points from the contour
     * @param contour contour to approximate
     * @param approximationAmount by how much to approximate
     * @return approximated contour
     */
    public static MatOfPoint getApproximatedContour(MatOfPoint contour, double approximationAmount)
    {
        MatOfPoint2f contourIn2f = new MatOfPoint2f(contour.toArray());
        double approximatedContourPerimeter = approximationAmount * Imgproc.arcLength(contourIn2f, true);
        MatOfPoint2f approximatedContourIn2f = new MatOfPoint2f();
        Imgproc.approxPolyDP(contourIn2f, approximatedContourIn2f, approximatedContourPerimeter, true);

        return new MatOfPoint(approximatedContourIn2f.toArray());
    }

    /**
     * Find the largest area contour from the list
     * @param contours contours list
     * @return largest area contour
     */
    public static MatOfPoint getLargestAreaContour(ArrayList<MatOfPoint> contours)
    {
        double largestArea = 0;
        MatOfPoint largestAreaContour = null;

        for(MatOfPoint contour : contours)
        {
            double contourArea = getContourArea(contour);
            if (largestArea < contourArea)
            {
                largestArea = contourArea;
                largestAreaContour = contour;
            }
        }

        return largestAreaContour;
    }

    /**
     * Calculates and returns contour area
     * @param contour contour
     * @return contour area
     */
    public static double getContourArea(MatOfPoint contour)
    {
        if(contour != null && contour.toArray().length > 2)
        {
            return Imgproc.contourArea(contour);
        }

        return 0;
    }

    /**
     * Finds all parent contours in the mat
     * @param matImage mat for finding contours
     * @return list of found contours
     */
    public static ArrayList<MatOfPoint> getParentContours(MatImage matImage)
    {
        MatImage temp = new MatImage();
        matImage.copyTo(temp);

        ArrayList<MatOfPoint> parentContours = new ArrayList<>();
        MatImage hierarchy = new MatImage();

        Imgproc.findContours(temp, parentContours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        return parentContours;
    }

    /**
     * Finds all contours in the mat
     * @param matImage mat for finding contours
     * @return list of found contours
     */
    public static ArrayList<MatOfPoint> getAllContours(MatImage matImage)
    {
        MatImage temp = new MatImage();
        matImage.copyTo(temp);

        ArrayList<MatOfPoint> allContours = new ArrayList<>();
        MatImage hierarchy = new MatImage();

        Imgproc.findContours(temp, allContours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        return allContours;
    }

    /**
     * Sorts contours by X and Y coordinates
     * @param contours contour list to sort
     * @param margin sorting margin
     */
    public static ArrayList<MatOfPoint> getSortedContoursByXY(ArrayList<MatOfPoint> contours, double margin)
    {
        Collections.sort(contours, new CompareByXWithMargin(margin));
        Collections.sort(contours, new CompareByYWithMargin(margin));

        return contours;
    }

    /**
     * Calculates and returns contour's centre of mass X coordinate
     * @param matOfPoint contour
     * @return Y coordinate
     */
    public static Double getXCoordinate(MatOfPoint matOfPoint)
    {
        Moments moments = Imgproc.moments(matOfPoint);

        return moments.get_m10() / moments.get_m00();
    }

    /**
     * Calculates and returns contour's centre of mass Y coordinate
     * @param matOfPoint contour
     * @return Y coordinate
     */
    public static Double getYCoordinate(MatOfPoint matOfPoint)
    {
        Moments moments = Imgproc.moments(matOfPoint);

        return  moments.get_m01() / moments.get_m00();
    }

    /**
     * Comparator class for comparing contours X coordinate with a margin
     */
    private static class CompareByXWithMargin implements Comparator<MatOfPoint>
    {
        private double sortingMargin = 0;

        public CompareByXWithMargin(double sortingMargin)
        {
            this.sortingMargin = sortingMargin;
        }

        @Override
        public int compare(MatOfPoint mP1, MatOfPoint mP2)
        {
            Double x1 = getXCoordinate(mP1);
            Double x2 = getXCoordinate(mP2);

            double deltaX = Math.abs(x1 - x2);
            if(deltaX <= sortingMargin) return 0;

            return x1.compareTo(x2);
        }
    }

    /**
     * Comparator class for comparing contours Y coordinate with a margin
     */
    private static class CompareByYWithMargin implements Comparator<MatOfPoint>
    {
        private double sortingMargin = 0;

        public CompareByYWithMargin(double sortingMargin)
        {
            this.sortingMargin = sortingMargin;
        }

        @Override
        public int compare(MatOfPoint mP1, MatOfPoint mP2)
        {
            Double y1 = getYCoordinate(mP1);
            Double y2 = getYCoordinate(mP2);

            double deltaY = Math.abs(y1 - y2);
            if(deltaY <= sortingMargin) return 0;

            return y1.compareTo(y2);
        }
    }

    /**
     * Comparator class for comparing contours by their area
     */
    private static class CompareByArea implements  Comparator<MatOfPoint>
    {
        @Override
        public int compare(MatOfPoint mP1, MatOfPoint mP2)
        {
            Double mP1Area = getContourArea(mP1);
            Double mP2Area = getContourArea(mP2);

            return mP1Area.compareTo(mP2Area);
        }
    }
}
