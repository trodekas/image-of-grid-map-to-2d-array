package opencvModule.core;

import opencvModule.dataStructure.MatImage;
import opencvModule.dataStructure.Matrix;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

/**
 * Created by Saulius Stankevicius.
 * Class for converting the mat with a grid into map matrix
 */
public class MainLoopCalculations
{
    private static final int ERODE_AMOUNT_FOR_GRID_SEARCH = 2;
    private static final int DILATE_AMOUNT_FOR_GRID_SEARCH = 4;

    private static final int ADAPTIVE_THRESHOLD_MAX_VALUE = 255;
    private static final int ADAPTIVE_THRESHOLD_BLOCK_SIZE = 5;
    private static final int ADAPTIVE_THRESHOLD_VALUE_TO_SUBTRACT_FROM_MEAN = 2;

    private static final double GRID_DETECTION_MARGIN_RATIO = 0.05;
    private static final double GRID_CONTOUR_APPROXIMATION_AMOUNT = 0.05;
    private static final double CELL_CONTOUR_APPROXIMATION_AMOUNT = 0.04;

    private static final double CELL_CONTOUR_SIMILARITY_AMOUNT = 0.9;

    private static final double SORTING_MARGIN_AMOUNT = 0.7;

    private static final int MIN_MATRIX_SIZE = 2;

    private MainLoopCalculations()
    {

    }

    /**
     * Finds and returns the contour of the grid, return null if not found
     * @param capturedMat mat to analyze
     * @return grid contour
     */
    public static MatOfPoint getGridContour(MatImage capturedMat)
    {
        MatImage preparedMat = getPreparedMat(capturedMat);
        MatImage thresholdMat = prepareMatForGridSearch(preparedMat);

        //Finds the largest contour and leaves only contour content
        MatOfPoint gridContour = ContourUtilities.getLargestAreaContour(ContourUtilities.getParentContours(thresholdMat));
        gridContour = ContourUtilities.getApproximatedContour(gridContour, GRID_CONTOUR_APPROXIMATION_AMOUNT);
        if(ContourUtilities.getContourArea(gridContour) > preparedMat.height() * preparedMat.width() * GRID_DETECTION_MARGIN_RATIO)
        {
            return gridContour;
        }

        return null;
    }

    /**
     * Analyzes Mat with a grid and returns calculated grid matrix
     * @param capturedMat to analyze
     * @param gridContour the contour of the grid
     * @return calculated grid matrix
     */
    public static Matrix<Integer> getCalculatedGridMatrix(MatImage capturedMat, MatOfPoint gridContour)
    {
        Matrix<Integer> matrix = null;

        //Prepares mat
        MatImage preparedMat = getPreparedMat(capturedMat);

        if(gridContour != null)
        {
            MatImage extractedContourMat = new MatImage();
            preparedMat.copyTo(extractedContourMat);
            //MatImage extractedContourMat = this.matImageProcessing.getOnlyContourContent(preparedMat, gridContour);
            MatImage gridMat = MatImageProcessing.getNormalizedPerspective(extractedContourMat, gridContour);

            MatImage gridThresholdMat = prepareMatForCellSearch(gridMat);

            ArrayList<MatOfPoint> gridMatApproxContours = ContourUtilities.getApproximatedContours(
                    ContourUtilities.getParentContours(gridThresholdMat), CELL_CONTOUR_APPROXIMATION_AMOUNT);
            double averageContourArea = ContourUtilities.getAverageContourArea(gridMatApproxContours);
            ArrayList<MatOfPoint> gridCellContours = ContourUtilities.getContoursWithSimilarAreaTo(gridMatApproxContours,
                    averageContourArea, CELL_CONTOUR_SIMILARITY_AMOUNT);
            double margin = Math.sqrt(averageContourArea) * SORTING_MARGIN_AMOUNT;
            gridCellContours = ContourUtilities.getSortedContoursByXY(gridCellContours, margin);

            matrix = getMapMatrix(gridMat, gridCellContours, margin);
        }

        return matrix;
    }

    /**
     * Fills the matrix
     * @param gridMat mat of grid
     * @param gridCellContours contours of cells
     * @param margin margin
     * @return filled with values matrix
     */
    private static Matrix<Integer> getMapMatrix(MatImage gridMat, ArrayList<MatOfPoint> gridCellContours, double margin)
    {
        Size matrixSize = getCalculatedMatrixSize(gridCellContours, margin);
        if (matrixSize.width > MIN_MATRIX_SIZE && matrixSize.height > MIN_MATRIX_SIZE)
        {
            Matrix<Integer> matrix = new Matrix<>(matrixSize);
            fillMatrix(matrix, gridMat, gridCellContours);

            return matrix;
        }

        return null;
    }

    /**
     * Fills the matrix with the results from ShapeRecognizer class
     * @param gridMat MatImage of the entire grid
     * @param cellContours A list of cells in the grid
     */
    private static void fillMatrix(Matrix<Integer> matrix, MatImage gridMat, ArrayList<MatOfPoint> cellContours)
    {
        int i = 0;
        for(int y = 0; y < matrix.getRows(); y++)
        {
            for(int x = 0; x < matrix.getCols(); x++)
            {
                String shape = getShapeInContour(cellContours.get(i), gridMat);
                i++;

                if(shape != null)
                {
                    switch (shape) {
                        case "RECTANGLE":
                            matrix.add(y, x, 1);
                            break;
                        case "TRIANGLE":
                            matrix.add(y, x, 2);
                            break;
                        default:
                            matrix.add(y, x, 0);
                            break;
                    }
                }
                else
                {
                    matrix.add(y, x, 0);
                }
            }
        }
    }

    /**
     * Returns a shape's name from cell contour
     * @param contour cell contour
     * @param gridMat greyscale and blurred grid mat
     * @return a name of a shape
     */
    private static String getShapeInContour(MatOfPoint contour, MatImage gridMat)
    {
        Rect boundingRect = Imgproc.boundingRect(contour);
        MatImage cellMat = new MatImage(gridMat.submat(boundingRect));

        Imgproc.adaptiveThreshold(cellMat, cellMat, ADAPTIVE_THRESHOLD_MAX_VALUE, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY, ADAPTIVE_THRESHOLD_BLOCK_SIZE, ADAPTIVE_THRESHOLD_VALUE_TO_SUBTRACT_FROM_MEAN);
        org.opencv.core.Core.bitwise_not(cellMat, cellMat);

        Mat structuringElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(4, 4));
        Imgproc.dilate(cellMat, cellMat, structuringElement);
        Imgproc.morphologyEx(cellMat, cellMat, Imgproc.MORPH_CLOSE, structuringElement);
        ArrayList<MatOfPoint> contours = ContourUtilities.getAllContours(cellMat);

        if (contours.size() > 0)
        {
            MatOfPoint largestContour = ContourUtilities.getLargestAreaContour(contours);

            if(ContourUtilities.checkIfContourAreaLargerThan(largestContour, boundingRect.area() * 0.1))
            {
                return ShapeRecognizer.getShapeName(largestContour);
            }
        }

        return null;
    }

    /**
     * Calculates and returns matrix dimensions
     * @param contours grid's cells contours
     * @param margin margin
     * @return matrix dimensions
     */
    private static Size getCalculatedMatrixSize(ArrayList<MatOfPoint> contours, double margin)
    {
        int matrixHeight = 1;

        for(int i = 1; i < contours.size(); i++)
        {
            double prevY = ContourUtilities.getYCoordinate(contours.get(i - 1));
            double currentY = ContourUtilities.getYCoordinate(contours.get(i));

            if(Math.abs(prevY - currentY) > margin)
            {
                matrixHeight++;
            }
        }

        int matrixWidth = contours.size() / matrixHeight;

        return new Size(matrixWidth, matrixHeight);
    }

    /**
     * Prepares and returns captured mat for analyzing
     * @return prepared mat
     */
    private static MatImage getPreparedMat(MatImage capturedMat)
    {
        MatImage preparedMat = new MatImage();
        capturedMat.copyTo(preparedMat);

        Imgproc.cvtColor(preparedMat, preparedMat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(preparedMat, preparedMat, new Size(11, 11), 0);

        return preparedMat;
    }

    /**
     * Prepares mat for grid searching
     * @param matImage prepared captured mat
     * @return threshold mat
     */
    private static MatImage prepareMatForGridSearch(MatImage matImage)
    {
        //Makes a threshold mat and applies basic morphological operations
        MatImage thresholdMat = new MatImage();
        Imgproc.adaptiveThreshold(matImage, thresholdMat, ADAPTIVE_THRESHOLD_MAX_VALUE, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY, ADAPTIVE_THRESHOLD_BLOCK_SIZE, ADAPTIVE_THRESHOLD_VALUE_TO_SUBTRACT_FROM_MEAN);
        org.opencv.core.Core.bitwise_not(thresholdMat, thresholdMat);
        thresholdMat = MatImageProcessing.getAppliedBasicMorphOperations(thresholdMat, ERODE_AMOUNT_FOR_GRID_SEARCH, DILATE_AMOUNT_FOR_GRID_SEARCH);

        return thresholdMat;
    }

    /**
     * Prepares and returns a mat for cell search
     * @param matImage normalized grid mat
     * @return prepared mat
     */
    private static MatImage prepareMatForCellSearch(MatImage matImage)
    {
        MatImage gridThresholdMat = new MatImage();
        Imgproc.adaptiveThreshold(matImage, gridThresholdMat, ADAPTIVE_THRESHOLD_MAX_VALUE, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY, ADAPTIVE_THRESHOLD_BLOCK_SIZE, ADAPTIVE_THRESHOLD_VALUE_TO_SUBTRACT_FROM_MEAN);

        //Applies morphological operations to grid mat
        Mat morphStruct = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(4, 4));
        Imgproc.erode(gridThresholdMat, gridThresholdMat, morphStruct);
        Imgproc.morphologyEx(gridThresholdMat, gridThresholdMat, Imgproc.MORPH_OPEN, morphStruct);

        return gridThresholdMat;
    }
}
